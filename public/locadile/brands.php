<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
	
	/* Wrappers, Overlays, Live-Search */
	include("inc/wrapper.php");
?>
    
    <!-- Flexslider -->
    <div id="flexslider-head" class="flexslider">
   
        <div class="wrapper">
            <div class="header-gallery-content">
            
                <div class="mb5">
                    <span class="button black-trans80 fs30 ttn">
                        KaDeWe
                    </span>
                </div>
                
                <div class="cf mb5">
                    <div class="float_l mr5">
                        <span class="button black-trans80 fs21 ttn">
                            Tauentzienstrasse 21-24
                        </span>
                    </div>
                    <div class="float_l">
                        <span class="button black-trans80 fs21 ttn">
                            10789 Berlin
                        </span>
                    </div>
                </div>
                
                <div class="cf mb5">
                    <div class="float_l mr5">
                        <a href="#" title="Email" class="button blue-trans80 fs18">
                            Email
                        </a>
                    </div>
                    <div class="float_l mr5">
                        <a href="#" title="Follow" class="button blue-trans80 fs18">
                            Follow
                        </a>
                    </div>
                    <div class="float_l">
                        <a href="#" title="Share" class="button blue-trans80 fs18">
                            Share
                        </a>
                    </div>                    
                </div> 
                
                <div class="mb5">
                    <span class="button white fs18 ttn open-now">Open now (2h 18m)</span>
                </div>
                
            </div>
        </div>   
        
        <!-- Header Gallery -->
        <ul class="slides cf">
            <li style="background-image: url(images/dummy/NewYork.jpeg);"></li>
            <li style="background-image: url(images/dummy/adidas_02.jpeg);"></li>
            <li style="background-image: url(images/dummy//AdidasStore.jpeg);"></li>
            <li style="background-image: url(images/dummy/Adidas-Store.jpeg);"></li>
                          
        </ul>
        <!-- // Header Gallery -->
   
    </div>
    <!-- // Flexslider -->
    
    <div class="fake-content hide-on-phones">
        <div class="row">
            <div class="twelve columns">
                <div id="flexslider-head-thumb">
                    
                    <ul class="slides cf">
                        <li style="background: url(images/dummy/NewYork.jpeg);"></li>
                        <li style="background: url(images/dummy/adidas_02.jpeg);"></li>
                        <li style="background: url(images/dummy/AdidasStore.jpeg);"></li>
                        <li style="background: url(images/dummy/Adidas-Store.jpeg);"></li>                        
                    </ul>
                    <!--
                        <li id="map"><a id="geolocate" href="#">Find me</a></li> 
                        <div style="background: url(images/map.jpg); width: 76px; height: 76px; float: right; margin-right: 35%;"></div>
                    -->
                </div>        
            </div>
        </div>    
    </div>
    
    <!-- Subnavigation -->
    <div class="container">
        <div class="row">
            <div class="twelve columns store-navigation">
                <a href="store.html" title="Info" class="button blue-trans80 fs18">Info</a>
                <a href="specials.html" title="Specials" class="button blue-trans80 fs18">Specials <span class="count">(4)</span></a>
                <a href="brands.html" title="Brands" class="button blue-trans80 fs18 active">Brands <span class="count">(42)</span></a>
            </div>
        </div>
    </div>
    
    <!-- Description -->
    <div class="row">
        <div class="twelve columns">
            
            <div class="filter">
                Filter Options come here
            </div>
        
            <ul class="block-grid four-up mobile" style="padding: 0px;">
                <li><img src="images/dummy/logo1-215x215.jpg" /></li>
                <li><img src="images/dummy/logo2-215x215.jpg" /></li>
                <li><img src="images/dummy/logo3-215x215.jpg" /></li>
                <li><img src="images/dummy/logo4-215x215.jpg" /></li>
                <li><img src="images/dummy/logo3-215x215.jpg" /></li>
                <li><img src="images/dummy/logo4-215x215.jpg" /></li>
                <li><img src="images/dummy/logo1-215x215.jpg" /></li>
                <li><img src="images/dummy/logo2-215x215.jpg" /></li>
                <li><img src="images/dummy/logo1-215x215.jpg" /></li>
                <li><img src="images/dummy/logo2-215x215.jpg" /></li>
                <li><img src="images/dummy/logo3-215x215.jpg" /></li>
                <li><img src="images/dummy/logo4-215x215.jpg" /></li>
                <li><img src="images/dummy/logo3-215x215.jpg" /></li>
                <li><img src="images/dummy/logo4-215x215.jpg" /></li>
                <li><img src="images/dummy/logo1-215x215.jpg" /></li>
                <li><img src="images/dummy/logo2-215x215.jpg" /></li>                
            </ul>
        </div>
    </div>
    <!-- // row -->	
<?php
	include_once("inc/footer.php");
	include_once("inc/foot.php");
?>