<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
	
	/* Wrappers, Overlays, Live-Search */
	include("inc/wrapper.php");
?>
    <!-- Flexslider -->
    <div id="flexslider-head" class="flexslider">
        
        <!-- Wrapper -->
        <div class="wrapper">
            <div class="header-gallery-content">
            
                <div class="mb5">
                    <span id="brand" class="button blue-trans80 fs30 ttn" style="cursor: pointer;">
                        Adidas <i class="icon-down-open" style="font-size: 18px; margin-top: -10px; position: relative; top: -2px;"></i>
                    </span>
                </div>
                
                <div class="cf mb5">
                    <div class="float_l mr5">
                        <a href="#" title="Email" class="button blue-trans80 fs18">
                            Email
                        </a>
                    </div>
                    <div class="float_l mr5">
                        <a href="#" title="Follow" class="button blue-trans80 fs18">
                            Follow
                        </a>
                    </div>
                    <div class="float_l">
                        <a href="#" title="Share" class="button blue-trans80 fs18">
                            Share
                        </a>
                    </div>                    
                </div> 
            </div>

        </div>   
        <!-- // wrapper -->

        
        <!-- Header Gallery -->
        <ul class="slides cf">
            <li style="background-image: url(images/dummy/messi.jpg);"></li>
            <li style="background-image: url(images/dummy/adidas_02.jpeg);"></li>
            <li style="background-image: url(images/dummy//AdidasStore.jpeg);"></li>
            <li style="background-image: url(images/dummy/Adidas-Store.jpeg);"></li>
                          
        </ul>
        <!-- // Header Gallery -->
   
    </div>
    <!-- // Flexslider -->
    
    <div class="fake-content hide-on-phones">
        <div class="row">
            <div class="twelve columns">
                <div id="flexslider-head-thumb">
                    
                    <ul class="slides cf">
                        <li style="background: url(images/dummy/messi.jpg);"></li>
                        <li style="background: url(images/dummy/adidas_02.jpeg);"></li>
                        <li style="background: url(images/dummy/AdidasStore.jpeg);"></li>
                        <li style="background: url(images/dummy/Adidas-Store.jpeg);"></li>                        
                    </ul>
                    
                </div>        
            </div>
        </div>    
    </div>
    
    <!-- Subnavigation -->
    <div class="container">
        <div class="row">
            <div class="twelve columns store-navigation">
                <a href="specials.html" title="Specials" class="button blue-trans80 fs18 active">Specials <span class="count">(4)</span></a>
                <a href="brands.html" title="Brands" class="button blue-trans80 fs18">Brands <span class="count">(42)</span></a>
            </div>
        </div>
    </div>
    
    <!-- Description -->
    <div class="row">
        <div class="twelve columns">
            <!-- Flexslider -->
            <div class="flexslider overflow-visible" style="margin-top: 0px;">
              <ul class="slides cf">

                <li>
                    <!-- item 1 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        A cool new Sneaker
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Release Party
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-1.jpg" />
                        </div>
                    </div>
                    
                    <!-- item 2 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        All 24
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Sports event
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-2.jpg" />
                        </div>
                    </div>              
                </li>
                
                <li>
                    <!-- item 1 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        A cool new Sneaker
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Release Party
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-1.jpg" />
                        </div>
                    </div>
                    
                    <!-- item 2 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        All 24
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Sports event
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-2.jpg" />
                        </div>
                    </div>              
                </li>
                
                <li>
                    <!-- item 1 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        A cool new Sneaker
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Release Party
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-1.jpg" />
                        </div>
                    </div>
                    
                    <!-- item 2 -->
                    <div class="slide-item">
                        <div class="slide-item-box">
                        
                            <!-- slide-item-text-box -->
                            <div class="slide-item-text-box">                            
                                <div class="mb5">
                                    <span class="text button black-trans80 fs24">
                                        All 24
                                    </span>
                                </div>
                                
                                <div class="mb5">
                                    <span class="button black-trans80 fs18">
                                        Sports event
                                    </span>
                                </div>                              
                               
                                <div class="cf mb5">
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">Freitag, 18.02.</span>
                                    </div>
                                    <div class="float_l mr5">
                                        <span class="button black-trans80 fs12 ttn">20:00h - 23:00h</span>
                                    </div>
                                    <div class="float_l">
                                        <a href="#" class="button blue-trans80 fs12">Share</a>
                                    </div>                    
                                </div> 
                            </div>
                            <!-- // slide-item-text-box -->
                            
                            <img src="images/special-2.jpg" />
                        </div>
                    </div>              
                </li>                
         
              </ul>            
            </div>
            <!-- // Flexslider -->
        </div>
    </div>
    <!-- // row -->	    
<?php
	include_once("inc/footer.php");
	include_once("inc/foot.php");
?>