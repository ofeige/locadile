    <!-- footer -->
    <footer class="container">
        <div class="row">
            <div class="twelve columns">
                <ul class="copyright" style="width: 232px;">
                    <li>
                        <span class="locadile">locadile.<span>com</span></span>
                        &copy 2012 Locadile Inc.
                    </li>
                </ul>
                <ul class="features-benefits" style="width: 183px;">
                    <li><strong>Product Features & Benefits</strong></li>
                    <li><a href="#" title="Plans and Pricing">Plans and Pricing</a></li>
                    <li><a href="#" title="Benefits">Benefits</a></li>
                    <li><a href="#" title="locadile for Business Owners">locadile for Business Owners</a></li>
                    <li><a href="#" title="locadile for Brands">locadile for Brands</a></li>
                    <li><a href="#" title="locadile for Agencies">locadile for Agencies</a></li>
                    <li><a href="#" title="locadile for Developers">locadile for Developers</a></li>
                    <li><a href="#" title="locadile for Enterprises">locadile for Enterprises</a></li>
                </ul>
                <ul class="customers-partners" style="width: 118px;">
                    <li><strong>Customers & Partners</strong></li>
                    <li><a href="#" title="Customers">Customers</a></li>
                    <li><a href="#" title="Partners">Partners</a></li>
                </ul>
                <ul class="about" style="width: 44px;">
                    <li><strong>About</strong></li>
                    <li><a href="#" title="About">About</a></li>
                    <li><a href="#" title="Contact">Contact</a></li>
                    <li><a href="#" title="Jobs">Jobs</a></li>
                </ul>
                <ul class="resources" style="width: 105px;">
                    <li><strong>Resources</strong></li>
                    <li><a href="#" title="Knowledge Base">Knowledge Base</a></li>
                    <li><a href="#" title="What is locadile?">What is locadile?</a></li>
                    <li><a href="#" title="Getting Started">Getting Started</a></li>
                    <li><a href="#" title="FAQ">FAQ</a></li>
                    <li><a href="#" title="API Documentation">API Documentation</a></li>
                    <li><a href="#" title="Blog">Blog</a></li>
                </ul>
                <ul class="other" style="width: 98px; margin-right: 0px;">
                    <li><strong>Other</strong></li>
                    <li><a href="#" title="About">Affiliate Programm</a></li>
                    <li><a href="#" title="Contact">Certified Partners</a></li>
                    <li><a href="#" title="Jobs">Request a Demo</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <!-- // footer -->