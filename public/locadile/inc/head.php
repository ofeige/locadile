<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="de"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="de"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="de"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="de"> <!--<![endif]-->
<head>
	<meta charset="utf-8">

	<!-- Set the viewport width to device width for mobile 
	<meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1; user-scalable=0;" />
	-->
	
	<title>locadile.com</title>

    <!-- Google fonts -->
    <link href="http://fonts.googleapis.com/css?family=Lato:400,100,700,300" rel="stylesheet" type="text/css">

	<!-- Included CSS Files -->
	<!-- Combine and Compress These CSS Files -->
	<link rel="stylesheet" href="stylesheets/grid.css">
	<link rel="stylesheet" href="stylesheets/ui.css">
	<link rel="stylesheet" href="stylesheets/forms.css">
	<!-- End Combine and Compress These CSS Files -->
	<link rel="stylesheet" href="stylesheets/app.css">

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="stylesheets/ie.css">
	<![endif]-->

    <!-- jQuery -->
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>

    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->



    <script src="javascripts/modernizr.foundation.js"></script>

    <!-- Included JS Files -->
    <!-- Combine and Compress These JS Files -->
    <script src="javascripts/jquery.placeholder.min.js"></script>
    <!-- End Combine and Compress These JS Files -->

    <!-- Nanoscroller -->
    <link rel="stylesheet" href="stylesheets/nanoscroller.css" type="text/css">
    <script src="javascripts/jquery.nanoscroller.min.js"></script>
    <script src="javascripts/jquery-debounce-min.js"></script>

    <!-- Flexslider -->
    <link rel="stylesheet" href="stylesheets/flexslider.css" type="text/css">
    <script src="javascripts/jquery.flexslider-min.js"></script>  

    <!-- Mapbox -->
    <script src='http://api.tiles.mapbox.com/mapbox.js/v0.6.6/mapbox.js'></script>
    <link href='http://api.tiles.mapbox.com/mapbox.js/v0.6.6/mapbox.css' rel='stylesheet' />

    <script src="javascripts/app.js"></script>

    <script src="javascripts/js_highlight.js"></script>
    <!-- JSB's BEGIN-->
    <script src="javascripts/JsBehaviourToolkit.js"></script>
    <script src="javascripts/Map.js"></script>
    <script src="javascripts/MapZoomControl.js"></script>
    <script src="javascripts/MapLocalization.js"></script>
    <script src="javascripts/Search.js"></script>
    <script src="javascripts/SearchSuggestion.js"></script>
    <script src="javascripts/Category.js"></script>
    <script src="javascripts/Tag.js"></script>
    <script src="javascripts/Special.js"></script>
    <script src="javascripts/Result.js"></script>

    <!-- JSB's END-->

    <script>
        $(document).ready(function () {
            $("#login-button").click(function () {
                $("#login").toggle();
            });
        });
    </script>
</head>
<body>