<div class="wrapper">

    <!-- Login -->
    <div id="login" class="overlay">
        <h2>Sign in</h2>

        <span class="seperator"></span>

        <form>
            <div class="input-login-holder input-frame username">
                <input type="text" placeholder="Username" />
            </div>
            <div class="input-login-holder input-frame password">
                <input type="password" placeholder="Password" />
            </div>
            <span class="button-nice">Sign <strong>in</strong></span>
            <a href="#" class="signup">or signup here</a>
        </form>

        <span style="margin-top: 30px!important;" class="seperator"></span>

        <div class="locadile-for">
            Locadile for <a href="#" title="business">business</a>, <a href="#">brands</a> or <a href="#">agencies</a>
        </div>
    </div>

    <!-- Track Location -->
    <div id="track-location" style="display: none;">
        <div class="head">
            <strong>Track location</strong>
            If you'd like to know more, read our <a href="#">privacy policy</a>.
        </div>
        <div class="content">
            or enter location manually
            <div class="input-location-holder input-frame search activ e">
                <input type="text" placeholder="Enter ZIP code or name" />
                <div>
                    <ul>
                        <li>Berlin</li>
                        <li>Germany</li>
                        <li>123</li>
                        <li>123</li>
                        <li>Berlin</li>
                        <li>Germany </li>
                        <li>123</li>
                        <li>123</li>
                        <li>Berlin</li>
                        <li>Germany</li>
                        <li>123</li>
                        <li>123</li>
                        <li>Berlin</li>
                        <li>Germany</li>
                        <li>123</li>
                        <li>123</li>         
                    </ul>
                </div>
            </div>
        </div>
    </div>
        
</div>



    <div class="wrapper">
		
		
            <!-- subbrands -->
            <div class="subbrands">
                <span class="ribbon">Subbrands</span>
                <span class="gradient"></span>
                
                    <!-- Nano Scrollbar -->
                    <div class="nano">                                        
                        <div class="content">   
                        
                            <ul class="subbrand-listing">
                                <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                                    <div>
                                        <h3>Adidas</h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-2.png);">
                                    <div>
                                        <h3>Adidas <span>Skateboarding</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>   
                                <li style="background-image: url(images/dummy/adidas-logo-3.png);">
                                    <div>
                                        <h3>Adidas <span>Originals</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-4.png);">
                                    <div>
                                        <h3>Adidas <span>Neo</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>      
                                <li style="background-image: url(images/dummy/adidas-logo-5.png);">
                                    <div>
                                        <h3>Adidas <span>SLVR</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-6.png);">
                                    <div>
                                        <h3>Adidas <span>Football</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>  
                                <li style="background-image: url(images/dummy/adidas-logo-7.png);">
                                    <div>
                                        <h3>Adidas <span>Performance</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-8.png);">
                                    <div>
                                        <h3>Adidas <span>Porsche Design</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>                                    <li style="background-image: url(images/dummy/adidas-logo-2.png);">
                                    <div>
                                        <h3>Adidas <span>Skateboarding</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>   
                                <li style="background-image: url(images/dummy/adidas-logo-3.png);">
                                    <div>
                                        <h3>Adidas <span>Originals</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-4.png);">
                                    <div>
                                        <h3>Adidas <span>Neo</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>      
                                <li style="background-image: url(images/dummy/adidas-logo-5.png);">
                                    <div>
                                        <h3>Adidas <span>SLVR</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-6.png);">
                                    <div>
                                        <h3>Adidas <span>Football</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>  
                                <li style="background-image: url(images/dummy/adidas-logo-7.png);">
                                    <div>
                                        <h3>Adidas <span>Performance</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li> 
                                <li style="background-image: url(images/dummy/adidas-logo-8.png);">
                                    <div>
                                        <h3>Adidas <span>Porsche Design</span></h3>
                                        <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Kleidung</span>
                                        <span class="category"><i class="icon-network dark-grey" title="Euro"></i> Sports Equipment</span>
                                    </div>
                                </li>                                
                            </ul>
                            
                        </div>
                    </div>
                    <!-- // Nano Scrollbar -->             
            </div>  
            <!-- subbrands -->  		
		
		
        <div class="notification-overlay">
            <span class="ribbon">Notafications</span>
            <span class="gradient"></span>

            <!-- Nano Scrollbar -->
            <div class="nano">
                <div class="content">

                    <ul class="notification-listing">
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                        <li style="background-image: url(images/dummy/adidas-logo-1.png);">
                            <div>
                                <h3>Adidas</h3>
                                <span class="category"><i class="icon-feather dark-grey" title="Euro"></i> Created a special "Happy new Year"</span>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- // Nano Scrollbar -->

        </div>    
    
    
        <!-- store-detail-overlay -->
        <form class="custom store-detail-overlay" method="" action="">
            <ul>
                <li class="yellowgreen">
                    <label for="show-specials"><i class="icon-star"></i> Show the specials <input type="checkbox" id="show-specials" /></label>
                </li>
                <li style="padding: 80px 0px 0px; ">
                    <span class="gradient"></span>
                    <span class="ribbon">Categories</span>

                    <!-- Nano Scrollbar -->
                    <div class="nano">
                        <div class="content">

                            <ul class="categories">
                                <li><label for="books-magazines"><i class="icon-erase"></i>Books & Magazines <input type="checkbox" id="books-magazines" /></label></li>
                                <li><label for="flowers-nature"><i class="icon-switch"></i>Flowers & Nature <input type="checkbox" id="flowers-nature" /></label></li>
                                <li><label for="travel"><i class="icon-infinity"></i>Travel <input type="checkbox" id="travel" /></label></li>
                                <li><label for="dinig-drinking"><i class="icon-home"></i>Dining & Drinking <input type="checkbox" id="dinig-drinking" /></label></li>
                                <li><label for="furniture"><i class="icon-keyboard"></i>Furniture <input type="checkbox" id="furniture" /></label></li>
                                <li><label for="fashion"><i class="icon-to-end"></i>Fashion <input type="checkbox" id="fashion" /></label></li>
                                <li><label for="robots-toys"><i class="icon-hourglass"></i>Robots & Toys <input type="checkbox" id="robots-toys" /></label></li>
                                <li><label for="books-magazines"><i class="icon-stop"></i>Books & Magazines <input type="checkbox" id="books-magazines" /></label></li>
                                <li><label for="flowers-nature"><i class="icon-switch"></i>Flowers & Nature <input type="checkbox" id="flowers-nature" /></label></li>
                                <li><label for="travel"><i class="icon-infinity"></i>Travel <input type="checkbox" id="travel" /></label></li>
                                <li><label for="dinig-drinking"><i class="icon-home"></i>Dining & Drinking <input type="checkbox" id="dinig-drinking" /></label></li>
                                <li><label for="furniture"><i class="icon-to-end"></i>Furniture <input type="checkbox" id="furniture" /></label></li>
                                <li><label for="fashion"><i class="icon-hourglass"></i>Fashion <input type="checkbox" id="fashion" /></label></li>
                                <li><label for="robots-toys"><i class="icon-stop"></i>Robots & Toys <input type="checkbox" id="robots-toys" /></label></li>
                            </ul>

                        </div>
                    </div>
                    <!-- // Nano Scrollbar -->

                </li>
                <li class="not-signed-in">
                    <i class="icon-user white" style="font-size: 28px; background-color: #181717; border-radius: 100%; width: 60px; height: 60px; display: block; text-align: center; float: left; margin-right: 16px; line-height: 52px;"></i>
                    You are not signed in.
                    <a href="#" class="sign-in" title="Sign in">
                        <i class="icon-pencil white" style="font-size: 32px; display: block; line-height: normal; margin-top: 20px; width: auto; margin-right: auto;"></i>
                        Sign in
                    </a>
                </li>
            </ul>
        </form>
        <!-- // store-detail-overlay -->
    </div>