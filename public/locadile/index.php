<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
	
	/* Wrappers, Overlays, Live-Search */
	include("inc/wrapper.php");
?>

    <header class="head" style="background-image: url(images/dummy/blur.jpg);">
        <div class="wrapper">
            <div class="start">

                <div class="mb5">
                    <span class="button black-trans80 fs24">
                        Local information
                    </span>
                </div>

                <div class="mb5">
                    <span class="button black-trans80 fs24">
                        right at your fingertips
                    </span>
                </div>

                <div class="mb5 cf">
                    <div class="float_l mr5">
                        <a href="https://itunes.apple.com/de/app/bvk-app/id527572550?mt=8" target="_blank" class="button blue-trans80 fs18 ttn">Download app</a>
                    </div>
                    <div class="float_l mr5">
                        <a href="map.php" class="button blue-trans80 fs18 ttn">Web Version</a>
                    </div>
                </div>
            </div>

            <div class="iphone-start">
                <div id="map" class="jsb_ jsb_map"></div>
            </div>
        </div>
    </header>

    <!-- Featured stores near you -->
    <div class="row">
        <div class="twelve columns featured">
            <h2><i class="icon-cog dark-grey" style="font-size: 32px; margin-right: 7px;"></i> Featured stores near you</h2>

            <!-- Flexslider -->
            <div class="flexslider overflow-visible loading">
                <ul class="slides cf">

                    <li>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">                               
                                
                                    <div class="mb5">
                                        <a href="store.php" title="The Prada Store" class="text button black-trans80 fs24">
                                            The Prada Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>                              

                                </div>
                                <img src="images/slide-1.jpg" alt="" />
                            </div>
                        </div>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">                               
                                
                                    <div class="mb5">
                                        <a href="store.php" title="Artemide Store" class="text button black-trans80 fs24">
                                            Artemide Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>                              

                                </div>
                                <img src="images/slide-2.jpg" alt="" />
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">
                                    <div class="mb5">
                                        <a href="store.php" title="Adidas Store" class="text button black-trans80 fs24">
                                            Adidas Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>  
                                </div>
                                <img src="images/slide-3.jpg" alt="" />
                            </div>
                        </div>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">
                                    <div class="mb5">
                                        <a href="store.php" title="The Prada Store" class="text button black-trans80 fs24">
                                            The Prada Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>    
                                </div>
                                <img src="images/slide-1.jpg" alt="" />
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">
                                    <div class="mb5">
                                        <a href="store.php" title="Artemide Store" class="text button black-trans80 fs24">
                                            The Artemide Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>   
                                </div>
                                <img src="images/slide-2.jpg" alt="" />
                            </div>
                        </div>
                        <div class="slide-item">
                            <div class="slide-item-box">
                                <div class="slide-item-text-box">
                                    <div class="mb5">
                                        <a href="store.php" title="Adidas Store" class="text button black-trans80 fs24">
                                            Adidas Store
                                        </a>
                                    </div>
                                    
                                    <div class="mb5">
                                        <a href="map.php" title="Show Stores in Berlin" class="button black-trans80 fs18">
                                            Berlin
                                        </a>
                                    </div>  
                                </div>
                                <img src="images/slide-3.jpg" alt="" />
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- // Flexslider -->
        </div>
    </div>
    <!-- // Featured stores near you -->

    <!-- Are you a business owner? Agency? Brand? -->
    <div class="white-bg">
        <div class="row">
            <div class="twelve columns get-started">
                <h2>Are you a business owner? Agency? Brand?</h2>
                <div class="subline">
                    <span class="blue fs18">See what locadile can do for you or</span> <a href="#" title="get started" class="button blue-trans80 ttn">get started</a>
                </div>

                <div class="row features123">
                    <div class="four columns">
                        <i class="icon-reply-all dark-grey" style="font-size: 96px;"></i><br />
                        <a href="#" title="get started" class="button blue-trans80">Feature 1</a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur elit. Integer faucibus, leo sit amet mattis interdum, nisl odio molestie neque eu.
                        </p>
                    </div>
                    <div class="four columns">
                        <i class="icon-ok dark-grey" style="font-size: 96px;"></i><br />
                        <a href="#" title="get started" class="button blue-trans80">Feature 2</a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur elit. Integer faucibus, leo sit amet mattis interdum, nisl odio molestie neque.
                        </p>
                    </div>
                    <div class="four columns">
                        <i class="icon-retweet dark-grey" style="font-size: 96px;"></i><br />
                        <a href="#" title="get started" class="button blue-trans80">Feature 3</a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur elit. Integer faucibus, leo sit amet mattis interdum, nisl odio molestie neque, eu commodo.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // Are you a business owner? Agency? Brand? -->





<?php
	include_once("inc/footer.php");
	include_once("inc/foot.php");
?>