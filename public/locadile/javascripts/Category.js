Category = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);
    
    this.initialize();
    this.initializeListener();
};

Category.prototype.initialize = function() {
    var that = this;

    that.list = '';
    that.category_array = ["Bücher & Zeitschriften", "Wine & Spirits", "Robots & Toys", "Fashion", "Reisen", "Möbel & Einrichtung", "Pflanzen & Blumen", "Lebensmittel"];
	that.category_input_result_wrapper = $('#category-input-result-wrapper');
    that.category_result_wrapper = $('#category-result-wrapper');
    that.category_input = $('#category-input');

    // put in fireevent
    that.createCategoryList(that.category_array);
    if(that.list.is('ul')) 
    {	
    	that.setEventToCategoryList();
    } 
};

Category.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('click', function(event) {
        that.onClick(event);
    });

   	jsb.on('Category::CHECKED', function(params) {
        that.getConsoleLog(params);
    });

    jsb.on('Result::CATEGORY', function(params) {
	    that.createCategoryList(that.category_array);
	    if(that.list.is('ul')) 
	    {	
	    	that.setEventToCategoryList();
	    } 
    });
};

Category.prototype.createCategoryList = function(categories) {
	var that = this;

	// noch ändern
	that.list = $('<ul></ul>');
	that.list.appendTo(that.category_result_wrapper);

	$(categories).each(function(index, category) {
		// that.list.append('<li data-value="' + {"id" : index, "value" : category} + '">' + category + '</li>')
		var item = $('<li>' + category + '</li>');
		item.attr('data-value', JSON.stringify({
			'id': index,
			'value': category
		}));
		that.list.append(item);
	});
};

Category.prototype.setEventToCategoryList = function() {
	var that = this;
	
	that.list.children().each(function(index, li) {
		$(li).click(function() {
			var category = $.parseJSON($(this).attr('data-value')).value

			that.setCategoryToInput(category);
		});
	});
};

Category.prototype.setCategoryToInput = function(category) {
	var that = this;

	console.log(category);
	that.category_input.text(category);
    that.onClick();
}

Category.prototype.getConsoleLog = function(params) {
	console.log(params);
};

Category.prototype.onClick = function() {
    var that = this;

    that.category_input_result_wrapper.toggleClass("focus");
    that.category_result_wrapper.slideToggle(250);
};

JsBehaviourToolkit.registerHandler('category', Category);