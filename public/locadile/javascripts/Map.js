Map = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);

    this.initialize();
    this.initializeListener();
};

Map.prototype.initialize = function() {
    var that = this;

    that.min_zoom_level = 11;
    that.max_zoom_level = 17;
        
    that.map = mapbox.map('map');
    that.map.setZoomRange(that.min_zoom_level, that.max_zoom_level);
    
    // Set iniital center and zoom
    that.map.centerzoom({
        lat: 52.5021588,
        lon: 13.3413228
    }, 15);

    that.map.addLayer(mapbox.layer().id('jrodriguez.map-459q3su2'));
};

Map.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('click', function() {
        that.onClick();
    });

    jsb.on('MapZoomControl::CLICKED', function(params) {
        that.setZoom(params);
    });

    // params {lat, lon}
    jsb.on('MapLocalization::LOCATION', function(params) {
        that.setLocation(params);
    });

    // params {lat, lon}
    jsb.on('RESULT::MAPPOINTS', function(params) {
        that.setMapPoints(params.points);
    });
};

Map.prototype.onClick = function(params) {
    var that = this;

    console.log('coordinateLocation', that.map.pointLocation({x: 0, y: 0}));
    console.log('coordinateLocation', that.map.pointLocation({x: that.map.dimensions.x, y: that.map.dimensions.y}));
};

Map.prototype.getCurrentZoom = function(params) {
    var that = this;

    var zoom = that.map.zoom() + params.level;

    if (zoom < that.min_zoom_level)
    {
        zoom = that.min_zoom_level;
    }
    else if (zoom > that.max_zoom_level)
    {
        zoom = that.max_zoom_level;
    }
    
    return zoom;    
};


Map.prototype.setZoom = function(params) {
    var that = this;

    var zoom = that.getCurrentZoom(params);
    that.map.zoom(zoom, true);

    if (zoom == that.min_zoom_level || zoom == that.max_zoom_level)
    {
        jsb.fireEvent('Map::SETCONTROLSTATUS', {'status': 'disabled'});
    }
    else
    {
        jsb.fireEvent('Map::SETCONTROLSTATUS', {'status': 'enabled'});   
    }
    
};

Map.prototype.setLocation = function(params) {
    var that = this;

    that.map.center({
      lat: params.lat,
      lon: params.lon
    });
};

Map.prototype.setMapPoints = function(points) {
    var that = this;

    $('.marker-image').remove();

    // Create and add marker layer
    var markerLayer = mapbox.markers.layer().features(points).factory(function(point) {
        // Define a new factory function. This takes a GeoJSON object
        // as its input and returns an element - in this case an image -
        // that represents the point.
        var img = document.createElement('img');
        img.className = 'marker-image';
        img.setAttribute('src', point.properties.icon);
        return img;
    });


    var interaction = mapbox.markers.interaction(markerLayer);
    that.map.addLayer(markerLayer);

    // Set a custom formatter for tooltips
    // Provide a function that returns html to be used in tooltip
    interaction.formatter(function(feature) {

        var o = ' ' +

            '<div class="map_point" style="position: absolute; top: 40px; left: 40px;"> ' +
            '   <div class="mb5">' +
            '       <span class="button black-trans80 fs21 ttn">'
                        +feature.properties.name+
            '       </span>' +
            '   </div>' +

            '    <div class="cf mb5">' +
            '        <div class="float_l mr5">' +
            '           <span class="button black-trans80 fs18 ttn">'
                            +feature.properties.addres+
            '           </span>' +
            '        </div>' +
            '        <div class="float_l">' +
            '           <span class="button black-trans80 fs18 ttn">'
                            +feature.properties.plzort+
            '           </span>' +
            '        </div>' +
            '    </div>' +

            '    <div class="cf mb5">' +
            '        <div class="float_l mr5">' +
            '           <span class="button black-trans80 fs18 ttn">'
                            +feature.properties.telefon+
            '           </span>' +
            '        </div>' +
            '        <div class="float_l">' +
            '           <a href="'+feature.properties.share+'" class="button blue-trans80 fs18 ">' +
            '                Share '+
            '           </a>' +
            '        </div>' +
            '    </div>' +

            '</div>' +

            '<img style="" src="'+feature.properties.image+'" style="display: block;" width="500" />'+
            '<a href="'+feature.properties.link+'" style="display:block; background-color: #009ddc; color: #fff; line-height: 45px; text-decoration: none; text-align: center; margin-top: 10px;">Show Details</a>';


        return o;
    });

};

JsBehaviourToolkit.registerHandler('map', Map);