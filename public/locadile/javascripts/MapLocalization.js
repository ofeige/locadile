MapLocalization = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);

    this.initialize();
    this.initializeListener();
};

MapLocalization.prototype.initialize = function() {
    var that = this;

    if (!navigator.geolocation) 
    {
        alert('Benutze Firefox oder Chrome oder am schlimmsten Fall IE-9!');
    }
    else
    {
        that.dom_element.bind('click', function() {
            that.onClick();
        });
    }
};

MapLocalization.prototype.initializeListener = function() {
    var that = this;


};

MapLocalization.prototype.onClick = function() {
    var that = this;

    that.getLocation();
};


MapLocalization.prototype.getLocation = function() {
    var that = this;

    navigator.geolocation.getCurrentPosition(
        function(position) {

            jsb.fireEvent('MapLocalization::LOCATION', {
                'lat': position.coords.latitude,
                'lon': position.coords.longitude
            });
        },

        function(err) {
            alert('Deine Position kann ich nicht finden!');
        }
    );
};

JsBehaviourToolkit.registerHandler('map_localization', MapLocalization);