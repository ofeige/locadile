MapZoomControl = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);

    this.initialize();
    this.initializeListener();
};

MapZoomControl.prototype.initialize = function() {
    var that = this;

    if ('zoom-in' == that.dom_element.attr('id'))
    {
        that.level = 1;
        that.negative_item = $('#zoom-out');
    }
    else
    {
        that.level = -1;
        that.negative_item = $('#zoom-in');
    }

};

MapZoomControl.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('click', function() {
        that.onClick();
    });

};

MapZoomControl.prototype.onClick = function() {
    var that = this;

    jsb.fireEvent('MapZoomControl::CLICKED', {'level' : that.level});

    jsb.on('Map::SETCONTROLSTATUS', function(params) {
        that.setStatus(params);
    });
};

MapZoomControl.prototype.setStatus = function(params) {
    var that = this;

    if ('disabled' == params.status)
    {
        that.dom_element.addClass('disabled');
        that.dom_element.css('border', '1px solid red');
    }
    else
    {
        that.dom_element.removeClass('disabled');
        that.dom_element.css('border', 'none');
    }

    that.negative_item.removeClass('disabled');
    that.negative_item.css('border', '1px solid green');
};

JsBehaviourToolkit.registerHandler('map_zoom_control', MapZoomControl);