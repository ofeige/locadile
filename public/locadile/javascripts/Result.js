Result = function(dom_element, options) {
    var that = this;

    this.initialize();
    this.initializeListener();
};

Result.prototype.initialize = function() {
    var that = this;

    that.result = [];
    that.search_term = '';
};

Result.prototype.initializeListener = function() {
    var that = this;

    jsb.on('SEARCH::RESULT', function(params) {
        that.search_term = params.search_term;
        that.setResult(params.result);
    });

    jsb.on('RESULT::REALRESULT', function() {
        that.setMapPoints();
        that.setTags();
        that.setSpecials();
    });
};

Result.prototype.setResult = function(result) {
    var that = this;
    var real_result = [];

    $(result).each(function(index, store){
        // Name-Compare
        if (-1 != store.name.toLowerCase().indexOf(that.search_term.toLowerCase()))
        {
            real_result.push(store);
        }
        else
        {
            // Search brands
            $(store.brands).each(function(index, brand){
                if (-1 != brand.toLowerCase().indexOf(that.search_term.toLowerCase()))
                {
                    real_result.push(store);
                }
            });
        }
    });

    that.result = real_result;

    jsb.fireEvent('RESULT::REALRESULT');
};

Result.prototype.setMapPoints = function() {
    var that = this;
    var points = [];
    /*
        {
            "geometry": {
                "type": "Point", 
                "coordinates": [13.3465228, 52.5025518]
            },
            "properties": {
                "name" : "Adidas HQ",
                "addres" : "Schiffbauerdamm 3",
                "plzort" : "10115 Berlin",
                "telefon" : "030 1234 5678 - 0",
                "share" : "",
                "entfernung" : "350m",
                "opening" : "Open til 8pm (2h 18m)",
                "link" : "store.html",
                "image": "overlay-1.jpg",
                "icon": "images/dummy/marker-adidas-blue.png"
            }
        }
    */
console.log('result -> ', that.result);
    $(that.result).each(function(index, store){
        points.push(
            {
                "geometry": {
                    "type": "Point", 
                    "coordinates": [store.long, store.lat]
                },
                "properties": {
                    "name" : store.name,
                    "addres" : store.address.street + ' ' + store.address.number,
                    "plzort" : store.address.zipcode + ' ' + store.address.city,
                    "telefon" : store.telefon.country_code + ' ' + store.telefon.area_code + ' ' + store.telefon.number,
                    "share" : "",
                    "entfernung" : "350m",
                    "opening" : "Open til 8pm (2h 18m)",
                    "link" : "store.php",
                    "image": store.images[0],
                    "icon": store.marker
                }                
            }
        );
    });
console.log('points -> ', points);
    jsb.fireEvent('RESULT::MAPPOINTS', {'points': points});

};

Result.prototype.setSpecials = function() {
    var that = this;

    var specials = [];

    $(that.result).each(function(index, store){
        $(store.specials).each(function(index, special){
            if (false === that.inArray(specials, special))
            {
                specials.push(special);
            }
        });
    });

    jsb.fireEvent('RESULT::SPECIALS', {'specials': specials});  
};

Result.prototype.setTags = function() {
    var that = this;
    var tags = [];

    $(that.result).each(function(index, store){
        $(store.tags).each(function(index, tag){
            if (false === that.inArray(tags, tag))
            {
                tags.push(tag);
            }
        });
    });

    jsb.fireEvent('RESULT::TAGS', {'tags': tags});  
};

Result.prototype.setCategories = function() {
    var that = this;

};

Result.prototype.inArray = function(arr, item) {
    var found_item = false;

    $(arr).each(function(index, arr_item){
        if (item == arr_item)
        {
            found_item = true;
        }
    });

    return found_item;
};