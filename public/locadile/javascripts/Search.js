SearchInput = function(dom_element, options) {
    var that = this;

    this.dom_element_wrapper = $(dom_element);
    this.dom_element = $('#search-input');

    this.initialize();
    this.initializeListener();
};

SearchInput.prototype.initialize = function() {
    var that = this;

    new Result();
    that.search_term = '';
    that.dom_element.focus();
    that.suggestions = ['Adidas', 'Adidas Store', 'Adidas Originals', 'Adidas Sport', 'Adidas Runnigs', 'Adidas Basketball', 'Adidas Football', 'Adina Hotel', 'Adina Watches', 'Adipose', 'Nike', 'Nike Air', 'Nike Store', 'Nikon', 'Apotheke', 'Bla', 'Blub'];

};

SearchInput.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('keyup', function(event) {
        that.onKeyUp(event);
    });

    jsb.on('SEARCHSUGGESTION::CLICKED', function(params) {
        that.setSearchTerm(params.search_term);
        that.sendSearchTerm();
    });

    jsb.on('SEARCHSUGGESTION::SELECTED', function(params) {
        that.setSearchTerm(params.search_term);
    });

};

SearchInput.prototype.onKeyUp = function(event) {
    var that = this;
    var value_length = that.dom_element.val().length;

    // ESCAPE
    if (27 == event.keyCode)
    {
        that.search_term = '';
        that.dom_element.val('');
        jsb.fireEvent('SEARCH::ESCAPED', {'event': event});
    }
    // ENTER
    else if (13 == event.keyCode)
    {
        jsb.fireEvent('SEARCH::ENTERED', {'event': event});
        that.sendSearchTerm();
    }
    // PAGE-UP and TOP
    else if (33 == event.keyCode || 38 == event.keyCode)
    {
        jsb.fireEvent('SEARCH::UP', {'event': event});
    }
    // PAGE-DOWN and BOTTOM
    else if (34 == event.keyCode || 40 == event.keyCode)
    {
        jsb.fireEvent('SEARCH::DOWN', {'event': event});
    }
    else
    {
        this.search_term = that.dom_element.val();
        if (value_length > 2)
        {
            that.getSearchSuggestoins();
        }
        else
        {
            jsb.fireEvent('SEARCH::NOSUGGESTION');
        }
    }
};

SearchInput.prototype.getSearchSuggestoins = function() {
    var that = this;
    var suggestion_array = [];

    for (var suggestion in that.suggestions)
    {
        if (-1 != that.suggestions[suggestion].toLowerCase().indexOf(this.search_term.toLowerCase()))
        {
            suggestion_array.push(that.suggestions[suggestion]);
        }
    };
    
    jsb.fireEvent('SEARCH::SUGGESTIONS', {'suggestions': suggestion_array, 'search_term': that.search_term});
};

SearchInput.prototype.sendSearchTerm = function() {
    var that = this;

    $.ajax({
        url: 'interface/stores.json',
        success: function(data){
            jsb.fireEvent('SEARCH::RESULT', {'result': data, 'search_term': that.search_term});
        },
        dataType: 'json'
    });
};

SearchInput.prototype.setSearchTerm = function(search_term) {
    var that = this;

    that.search_term = search_term;
    that.dom_element.val(that.search_term);
};

JsBehaviourToolkit.registerHandler('search_input', SearchInput);