SearchSuggestion = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);
    
    this.initialize();
    this.initializeListener();
};

SearchSuggestion.prototype.initialize = function() {
    var that = this;

    that.suggestion_list = [];
    that.suggestion_list_items = [];
    that.suggestion_list_items_index = -1;
    that.current_element = null;
    that.search_term = '';
};

SearchSuggestion.prototype.initializeListener = function() {
    var that = this;

    jsb.on('SEARCH::UP', function(params) {
        that.up(params.event);
    });

    jsb.on('SEARCH::DOWN', function(params) {
        that.down(params.event);
    });

    jsb.on('SEARCH::ESCAPED', function(params){
        that.escaped(params.event);
    });
    
    jsb.on('SEARCH::ENTERED', function(params) {
        that.slideUp();
    });

    jsb.on('SEARCH::NOSUGGESTION', function(){
        that.escaped();
    });

    jsb.on('SEARCH::SUGGESTIONS', function(params) {
        that.suggestion_list = params.suggestions;
        that.search_term = params.search_term;
        that.createSuggestionList();
        that.highlightSuggestionList();
        that.setSuggestionListEvent();
    });
};


SearchSuggestion.prototype.createSuggestionList = function(event) {
    var that = this;

    that.suggestion_list_items = [];
    that.suggestion_list_items_index = -1;
    var count_suggestion_list = that.suggestion_list.length;
    that.list = $('<ul></ul>');

    $(that.suggestion_list).each(function(index, suggestion){
        var li = $('<li data-value="' + suggestion + '">' + suggestion + '</li>')
        that.suggestion_list_items.push(li);
        li.appendTo(that.list);
    });

    if (0 != count_suggestion_list)
    {
        that.dom_element.html(that.list);
        that.slideDown();
    }
    else
    {
        that.slideUp();
    }
};

SearchSuggestion.prototype.highlightSuggestionList = function(event) {
    var that = this;

    that.list.children('li').highlight(that.search_term);
};

SearchSuggestion.prototype.setSuggestionListEvent = function(event) {
    var that = this;

    that.list.children('li').each(function(index, li){
        li = $(li);
        li.click(function(){
            that.slideUp();
            jsb.fireEvent('SEARCHSUGGESTION::CLICKED', {'search_term': li.attr('data-value')});
        });
    });
};

SearchSuggestion.prototype.escaped = function(event) {
    var that = this;

    that.slideUp();
};

SearchSuggestion.prototype.setActiveListItem = function(event) {
    var that = this;
    var list_item = that.suggestion_list_items[that.suggestion_list_items_index];

    that.list.children('li').removeClass('active');
    list_item.addClass('active');

    jsb.fireEvent('SEARCHSUGGESTION::SELECTED', {'search_term': list_item.attr('data-value')});
    
};

SearchSuggestion.prototype.up = function(event) {
    var that = this;

    that.suggestion_list_items_index -= 1;

    if (0 > that.suggestion_list_items_index)
    {
        that.suggestion_list_items_index = that.suggestion_list_items.length - 1;
    }

    that.setActiveListItem(event);
};

SearchSuggestion.prototype.down = function(event) {
    var that = this;

    that.suggestion_list_items_index += 1;

    if (0 > that.suggestion_list_items_index || that.suggestion_list_items_index == that.suggestion_list_items.length)
    {
        that.suggestion_list_items_index = 0;
    }

    that.setActiveListItem(event);
};

SearchSuggestion.prototype.slideUp = function() {
    var that = this;

    that.dom_element.parent().removeClass("focus");
    that.dom_element.slideUp(250);
};

SearchSuggestion.prototype.slideDown = function() {
    var that = this;

    that.dom_element.parent().addClass("focus");
    that.dom_element.slideDown(250);
};

JsBehaviourToolkit.registerHandler('search_suggestion', SearchSuggestion);