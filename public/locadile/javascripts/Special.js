Special = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);
    
    this.initialize();
    this.initializeListener();
};

Special.prototype.initialize = function() {
    var that = this;

    that.list = '';
    that.special_array = ["Sales", "Special opening Hours", "Slash Sale / Popup Store", "Coupons", "Neueröffnung"];
	that.specialfilter_button_result_wrapper = $('#specialfilter-button-result-wrapper');
    that.specialfilter_result_wrapper = $('#specialfilter-result-wrapper');

    // put in fireevent
    that.createSpecialList(that.special_array);
    if(that.list.is('ul')) 
    {
    	that.setEventToSpecialList();
    }
};

Special.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('click', function(event) {
        that.onClick(event);
    });

   	jsb.on('Special::CHECKED', function(params) {
        that.getConsoleLog(params);
    });

    jsb.on('Result::SPECIAL', function(params) {
	    that.createSpecialList(params.specials);
	    if(that.list.is('ul')) 
	    {
	    	that.setEventToSpecialList();
	    }
    });
};

Special.prototype.createSpecialList = function(specials) {
	var that = this;

	that.list = $('<ul></ul>');
	that.list.appendTo(that.specialfilter_result_wrapper);

	$(specials).each(function(index, special) {
		that.list.append('<li data-id="' + index +'">' + special + '<span class="checkbox checked right"></span></li>')
	});
};

Special.prototype.setEventToSpecialList = function() {
	var that = this;

	that.list.children().each(function(index, li) {
		$(li).click(function() {
			
			var span = $(this).find('span.checkbox').toggleClass('checked');

			if(span.hasClass('checked')) 
			{
				jsb.fireEvent('Special::CHECKED', {
					'checked' : true, 
					'special': $(this).attr('data-id')
				});
			}
			else {
				jsb.fireEvent('Special::CHECKED', {
					'checked' : false, 
					'special': $(this).attr('data-id')
				});
			}
		});
	});
};

Special.prototype.getConsoleLog = function(params) {
	console.log(params);
};

Special.prototype.onClick = function() {
    var that = this;

    that.specialfilter_button_result_wrapper.toggleClass("focus");
    that.specialfilter_result_wrapper.slideToggle(250);
};

JsBehaviourToolkit.registerHandler('special', Special);