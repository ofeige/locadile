Tag = function(dom_element, options) {
    var that = this;

    this.dom_element = $(dom_element);
    
    this.initialize();
    this.initializeListener();
};

Tag.prototype.initialize = function() {
    var that = this;

    that.list = '';
    that.tagfilter_button_result_wrapper = $('#tagfilter-button-result-wrapper');
    that.tagfilter_result_wrapper = $('#tagfilter-result-wrapper');
};

Tag.prototype.initializeListener = function() {
    var that = this;

    that.dom_element.bind('click', function(event) {
        that.onClick(event);	
    });

   	jsb.on('Tag::CHECKED', function(params) {
        that.getConsoleLog(params);
    });

    jsb.on('RESULT::TAGS', function(params) {
	    that.createTagList(params.tags);
    });
};

Tag.prototype.createTagList = function(tags) {
	var that = this;

	that.tagfilter_result_wrapper.empty();

	// noch ändern
	that.list = $('<ul class="clearfix tags"></ul>');
	that.list.appendTo(that.tagfilter_result_wrapper);

	$(tags).each(function(index, tag) {
		that.list.append('<li data-value="' + tag +'">' + tag + '</li>')
	});

    if(that.list.is('ul.tags')) 
    {	
    	that.setEventToTagList();
    } 	
};

Tag.prototype.setEventToTagList = function() {
	var that = this;
	
	that.list.children().each(function(index, li) {
		$(li).click(function() {

			var li = $(this).toggleClass('checked');

			if($(this).hasClass('checked')) 
			{
				jsb.fireEvent('Tag::CHECKED', {
					'checked' : true, 
					'tag': $(this).attr('data-value')
				});
			}
			else {
				jsb.fireEvent('Tag::CHECKED', {
					'checked' : false, 
					'tag': $(this).attr('data-value')
				});
			}
		});
	});
};

Tag.prototype.getConsoleLog = function(params) {
	console.log(params);
};

Tag.prototype.onClick = function() {
    var that = this;

    that.tagfilter_button_result_wrapper.toggleClass("focus");
    that.tagfilter_result_wrapper.slideToggle(250);
};

JsBehaviourToolkit.registerHandler('tag', Tag);