$(window).load(function() {

    
    $('#showMap').click(function() {
        $('#map').toggle();
    });
    $('#flexslider-head-thumb').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 76,
        itemMargin: 10,
        asNavFor: '#flexslider-head'               
    });

    $('#flexslider-head').flexslider({
        animation: "slide",
        controlNav: false,
        sync: "#flexslider-head-thumb"        
    });
    
    $('.flexslider').flexslider({
        animation: "slide"
    });
    
    
    /*            
  $('.carsoul').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: true,
    directionNav: true, 
    
    itemWidth: 215,
    itemMargin: 40
  });*/
                
}); 


jQuery(document).ready(function ($) {
    $('#brand').click(function() {   
        $(this).toggleClass('active');
        
        /*if ( $('.subbrands').is(':hidden') ) {
            $('.subbrand-listing li').hide();
        }*/
        
        $(".subbrands").fadeToggle('fast', function() {
            var intTime = 0;
            $('.subbrand-listing li').each(function(i){
                var _this = this;
                window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
                intTime += 150;
            });              
        });
        
        $(".nano").nanoScroller({ iOSNativeScrolling: true });
    });

    $('.notification').click(function() {   
        $(this).toggleClass('active');
        
        /*if ( $('.subbrands').is(':hidden') ) {
            $('.subbrand-listing li').hide();
        }*/
        
        $(".notification-overlay").fadeToggle('fast', function() {
            var intTime = 0;
            $('.notification-listing li').each(function(i){
                var _this = this;
                window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
                intTime += 150;
            });              
        });
        
        $(".nano").nanoScroller({ iOSNativeScrolling: true });
    });    
    
    $(".icon-down-bold").click(function() {
        $(this).toggleClass('active');
        $(".store-detail-overlay").fadeToggle('fast', function() {
        
        /*var intTime = 0;
        $('.categories li').each(function(i){
            var _this = this;
            window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
            intTime += 150;
        });        
        */
            $(".nano").nanoScroller({ iOSNativeScrolling: true });
        });
    });
    
    
   $(".nano").nanoScroller({ iOSNativeScrolling: true });
    
    // 
    
    
	/* Use this js doc for all application specific JS */

	/* TABS --------------------------------- */
	/* Remove if you don't need :) */

	function activateTab($tab) {
		var $activeTab = $tab.closest('dl').find('a.active'),
				contentLocation = $tab.attr("href") + 'Tab';
				
		// Strip off the current url that IE adds
		contentLocation = contentLocation.replace(/^.+#/, '#');

		//Make Tab Active
		$activeTab.removeClass('active');
		$tab.addClass('active');

    //Show Tab Content
		$(contentLocation).closest('.tabs-content').children('li').hide();
		$(contentLocation).css('display', 'block');
	}

  $('dl.tabs dd a').on('click', function (event) {
    activateTab($(this));
  });

	if (window.location.hash) {
		activateTab($('a[href="' + window.location.hash + '"]'));
		$.foundation.customForms.appendCustomMarkup();
	}

	/* ALERT BOXES ------------ */
	$(".alert-box").delegate("a.close", "click", function(event) {
    event.preventDefault();
	  $(this).closest(".alert-box").fadeOut(function(event){
	    $(this).remove();
	  });
	});


	/* PLACEHOLDER FOR FORMS ------------- */
	/* Remove this and jquery.placeholder.min.js if you don't need :) */

	$('input, textarea').placeholder();

	/* TOOLTIPS ------------ */
	// $(this).tooltips();



	/* UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE6/7/8 SUPPORT AND ARE USING .block-grids */
//	$('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'left'});
//	$('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'left'});
//	$('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'left'});
//	$('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'left'});



	/* DROPDOWN NAV ------------- */

	var lockNavBar = false;
	$('.nav-bar a.flyout-toggle').on('click', function(e) {
		e.preventDefault();
		var flyout = $(this).siblings('.flyout');
		if (lockNavBar === false) {
			$('.nav-bar .flyout').not(flyout).slideUp(500);
			flyout.slideToggle(500, function(){
				lockNavBar = false;
			});
		}
		lockNavBar = true;
	});
  if (!Modernizr.touch) {
    $('.nav-bar>li.has-flyout').hover(function() {
      $(this).children('.flyout').show();
    }, function() {
      $(this).children('.flyout').hide();
    })
  }


	/* DISABLED BUTTONS ------------- */
	/* Gives elements with a class of 'disabled' a return: false; */
  
  /* CUSTOM FORMS */
  // $.foundation.customForms.appendCustomMarkup();
  
  
});
