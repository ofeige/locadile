Map = function(dom_element, options) {
    var that = this;

    this.dom_element = dom_element;
    this.initializeListener();
};

Map.prototype.initializeListener = function() {
    var that = this;

    jQuery(that.dom_element).bind('click', function() {
        that.onClick();            
    });
};

Map.prototype.onClick = function() {
    jsb.fireEvent('Map::CLICKED', {"date": new Date().toString()});
};


JsBehaviourToolkit.registerHandler('map', Map);