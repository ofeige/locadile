$(document).ready(function(){
 
    $.closeLiveSearch = function(opt) {
            
        if(opt == "all") {
            $('#search-result').slideUp(250, function() {
                $('#search-input-holder').slideUp(250, function () {
                    $('#live-search-input').val("");
                });
            });
        }
        else if (opt == "search-result") {
            $('#search-result').slideUp(250);
        } else {
            $('#search-result').slideUp(250, function() {
                $('#search-input-holder').slideUp(250, function () {
                    $('#live-search-input').val("");
                });
            });                
        }

        return this;
    }
    
    $.startLiveSearch = function() {
        
        $('#search-input-holder').slideDown(250, function(e) {
            
            
            $("#live-search-input").focus(); /* Fokus auf Input richtigen, NACH Animation */
            $("#live-search-input").bind("keyup", function(event) {
    
   
            
                /*
                var search = {
                    topEntry: ["Adidas"],
                    stores: ["Adidas", "Puma", "Nike", "Reebock"],
                    brands: ["Brands"],
                    adresses: ["Invalidenstraße 11, 12345 Berlin", "lol, 7gdza"]
                }*/
                
                // console.log(search);            
                
                // 4 Suchbereiche
                
                var searchTopEntry = new Array("Adidas", "Adi", "Adidas Performance", "Adidosis", "Adidas Skateboarding", "Adidas Originals", "Adidas Popadidas", "Adidas Store", "Adios", "Amiko", "Ahdnu", "Lorem", "Ipsum", "Dolor", "Sit", "Amet", "Digramm Media GmbH", "KaDeWe", "Nike", "Smart", "Audi", "BMW");
                var searchStroes = new Array("Adidas Performance Store", "Adidas Originals Store", "Adidas Soccer Store", "Adidas Popadidas Store", "Adidas Skateboarding Store");
                var searchBrands = new Array("Brand 1", "Brand 2", "Brand 3", "Brand 4", "Brand 5", "Brand 6");
                var searchAdresses = new Array("Adressestraße 12, 12345 Berlin", "Invalidenstraße 32, 11015 Berlin", "Seestraße 11, 13462 Berlin");
            
				$('#search-result ul').html(''); // Listen leeren
				$('.search-more').html(''); 
                    
                var typedText = $(this).val();
                    
                if(typedText.length > 2) {
                    var searchResultTempHeight = $(window).height();
                    $('#search-result').css("height", searchResultTempHeight-188);                        
                    $('#search-result').slideDown(250);
                    
                    var search = new RegExp(typedText, "gi");
                    
                    // Topresults
                        var resultsTop = [];
                        counterTop = 0;
                        for (var i = 0; i < searchTopEntry.length; i++){
                            if (searchTopEntry[i].match(search)){
                                resultsTop[counterTop] = searchTopEntry[i];
                                counterTop++;
                            }
                        }
                        if(searchTopEntry.length > 0){
                            for (var i = 0; i < resultsTop.length; i++) {
                                if(i < 3) {
                                    $('#search-top-entry ul').append('<li>'+resultsTop[i]+'</li>');
                                }
                            }
                            i = i-3;
                        }
                        if(i > 2) {
                            $('#search-top-entry').append('<span class="search-more">+ '+ i +' more</span>');
                        }
                    
					
                    // searchStroes
                        var resultsStores = [];
                        counterStores = 0;
                        for (var i = 0; i < searchStroes.length; i++){
                            if (searchStroes[i].match(search)){
                                resultsStores[counterStores] = searchStroes[i];
                                counterStores++;
                            }
                        }
                        if(searchStroes.length > 0){
                            for (var i = 0; i < resultsStores.length; i++) {
                                if(i < 3) {
                                    $('#search-stores ul').append('<li>'+resultsStores[i]+'</li>');
                                }
                            }
                            i = i-3;
                        }
                        if(i > 2) {
                            $('#search-stores').append('<span class="search-more">+ '+ i +' more</span>');
                        }
 
                    // searchBrands
                        var resultsBrands = [];
                        counterBrands = 0;
                        for (var i = 0; i < searchBrands.length; i++){
                            if (searchBrands[i].match(search)){
                                resultsBrands[counterBrands] = searchBrands[i];
                                counterBrands++;
                            }
                        }
                        if(searchBrands.length > 0){
                            for (var i = 0; i < resultsBrands.length; i++) {
                                if(i < 3) {
                                    $('#search-brands ul').append('<li>'+resultsBrands[i]+'</li>');
                                }
                            }
                            i = i-3;
                        }
                        if(i > 2) {
                            $('#search-brands').append('<span class="search-more">+ '+ i +' more</span>');
                        } 
                 
                    // searchAdresses
                        var resultsAdresses = [];
                        counterAdresses = 0;
                        for (var i = 0; i < searchAdresses.length; i++){
                            if (searchAdresses[i].match(search)){
                                resultsAdresses[counterAdresses] = searchAdresses[i];
                                counterAdresses++;
                            }
                        }
                        if(searchAdresses.length > 0){
                            for (var i = 0; i < resultsAdresses.length; i++){
                                if(i < 3) {
                                    $('#search-adressen ul').append('<li>'+resultsAdresses[i]+'</li>');
                                }
                            }
                            i = i-3;
                        }
                        if(i > 2) {
                            $('#search-adressen').append('<span class="search-more">+ '+ i +' more</span>');                         
                        }
                        
                    // highlightscript
                    $("#search-result ul li").highlight(typedText);
                    
            if (event.keyCode == 27) {
                $.closeLiveSearch("all");
            }                     
                    
                } else {
                    // $.closeLiveSearch("search-result");
                }
	
                
            });
            
        });        
        
        return this;
    }
  
    /* Klick auf Search-Icon */
    $("#searchController").click(function () {
        $.startLiveSearch();
    }); 
    $("#search-result").on("click", ".search-more", function() {
		$.closeLiveSearch("search-result");
		var searchResultTempHeight = $(window).height();
		$('#search-result-more').css("height", searchResultTempHeight-188);  		
		$("#search-result-more").show();
	});
    $(".search-close").click(function () {
        $.closeLiveSearch("all");
    });        
    
	
    
// var m = mapbox.map('map').zoom(11).center({ lat: 52.5008061, lon: 13.3890266 });

    // m.addLayer(mapbox.layer().id('jrodriguez.map-ttrvg86r'));

/*

    // GeoJSON input features
    // The image and url properties of the features will be used in
    // the tooltips
    var features = [{
        "geometry": { "type": "Point", "coordinates": [13.3413228, 52.5021588]},
        "properties": {
            "name" : "Adidas NEO Store",
            "addres" : "Schiffbauerdamm 3",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-1.jpg",
            "icon": "images/dummy/marker-adidas.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3465228, 52.5025518]},
        "properties": {
            "name" : "Adidas HQ",
            "addres" : "Schiffbauerdamm 3",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-1.jpg",
            "icon": "images/dummy/marker-adidas-blue.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3365228, 52.5005518]},
        "properties": {
            "name" : "Edika",
            "addres" : "Nürnbergerstr. 41",
            "plzort" : "10815 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "1.7km",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-3.jpg",
            "icon": "images/dummy/marker-edika.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3305228, 52.5005518]},
        "properties": {
            "name" : "Edika",
            "addres" : "Nürnbergerstr. 41",
            "plzort" : "10815 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "1.7km",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-3.jpg",
            "icon": "images/dummy/marker-edika.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3355228, 52.5035518]},
        "properties": {
            "name" : "Edika",
            "addres" : "Nürnbergerstr. 41",
            "plzort" : "10815 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "1.7km",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-3.jpg",
            "icon": "images/dummy/marker-edika.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3398228, 52.5055518]},
        "properties": {
            "name" : "Karstadt",
            "addres" : "Budapesterstr. 83",
            "plzort" : "14059 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "800m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-2.jpg",
            "icon": "images/dummy/marker-karstadt.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3498228, 52.5068518]},
        "properties": {
            "name" : "NEO Store",
            "addres" : "Corneliusstr. 5",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-5.jpg",
            "icon": "images/dummy/marker-neo.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates": [13.3378228, 52.5035518]},
        "properties": {
            "name" : "NEO Store",
            "addres" : "Corneliusstr. 5",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-5.jpg",
            "icon": "images/dummy/marker-neo.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates":  [13.3498228, 52.5028518]},
        "properties": {
            "name" : "Telekom",
            "addres" : "Kurfürstenstr 3",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-4.jpg",
            "icon": "images/dummy/marker-telekom.png"
        }
    }, {
        "geometry": { "type": "Point", "coordinates":  [13.3398228, 52.5028518]},
        "properties": {
            "name" : "Telekom",
            "addres" : "Kurfürstenstr 3",
            "plzort" : "10115 Berlin",
            "telefon" : "030 1234 5678 - 0",
            "share" : "",
            "entfernung" : "350m",
            "opening" : "Open til 8pm (2h 18m)",
            "link" : "store.html",
            "image": "overlay-4.jpg",
            "icon": "images/dummy/marker-telekom.png"
        }
    }];

    // Create map
    var map = mapbox.map('map');
    map.addLayer(mapbox.layer().id('jrodriguez.map-ttrvg86r'));

    // Create and add marker layer
    var markerLayer = mapbox.markers.layer().features(features).factory(function(feature) {        
        // Define a new factory function. This takes a GeoJSON object
        // as its input and returns an element - in this case an image -
        // that represents the point.
        var img = document.createElement('img');
        img.className = 'marker-image';
        img.setAttribute('src', feature.properties.icon);
        return img;
    });
    
    
    var interaction = mapbox.markers.interaction(markerLayer);
    map.addLayer(markerLayer);

    // Set a custom formatter for tooltips
    // Provide a function that returns html to be used in tooltip
    interaction.formatter(function(feature) {
   
        var o = ' ' + 
        
            '<div class="" style="position: absolute; top: 40px; left: 40px;"> ' +
            '   <div class="mb5">' +
            '       <span class="button black-trans80 fs21 ttn">' 
                        +feature.properties.name+
            '       </span>' +
            '   </div>' +
                
            '    <div class="cf mb5">' +
            '        <div class="float_l mr5">' +
            '           <span class="button black-trans80 fs18 ttn">' 
                            +feature.properties.addres+
            '           </span>' +
            '        </div>' +
            '        <div class="float_l">' +
            '           <span class="button black-trans80 fs18 ttn">' 
                            +feature.properties.plzort+
            '           </span>' +
            '        </div>' +
            '    </div>' +
           
            '    <div class="cf mb5">' +
            '        <div class="float_l mr5">' +
            '           <span class="button black-trans80 fs18 ttn">' 
                            +feature.properties.telefon+
            '           </span>' +
            '        </div>' +
            '        <div class="float_l">' +
            '           <a href="'+feature.properties.share+'" class="button blue-trans80 fs18 ">' + 
            '                Share '+
            '           </a>' +
            '        </div>' +
            '    </div>' +           
         
            '</div>' +
                        
            '<img style="" src="images/dummy/'+feature.properties.image+'" style="display: block;" />'+
            '<a href="'+feature.properties.link+'" style="display:block; background-color: #009ddc; color: #fff; line-height: 45px; text-decoration: none; text-align: center; margin-top: 10px;">Show Details</a>';
         
            
        return o;
    });
    
    map.setZoomRange(7, 17);
    // Set iniital center and zoom
    map.centerzoom({
        lat: 52.5021588,
        lon: 13.3413228
    }, 15);      
    
*/

});

$(window).load(function() {

    
    $('#showMap').click(function() {
        $('#map').toggle();
    });
    $('#flexslider-head-thumb').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 76,
        itemMargin: 10,
        asNavFor: '#flexslider-head'               
    });

    $('#flexslider-head').flexslider({
        animation: "slide",
        controlNav: false,
        sync: "#flexslider-head-thumb"        
    });
    
    $('.flexslider').flexslider({
        animation: "slide"
    });
    
    
    /*            
  $('.carsoul').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: true,
    directionNav: true, 
    
    itemWidth: 215,
    itemMargin: 40
  });*/
                
}); 


jQuery(document).ready(function ($) {
    $('#brand').click(function() {   
        $(this).toggleClass('active');
        
        /*if ( $('.subbrands').is(':hidden') ) {
            $('.subbrand-listing li').hide();
        }*/
        
        $(".subbrands").fadeToggle('fast', function() {
            var intTime = 0;
            $('.subbrand-listing li').each(function(i){
                var _this = this;
                window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
                intTime += 150;
            });              
        });
        
        $(".nano").nanoScroller({ iOSNativeScrolling: true });
    });

    $('.notification').click(function() {   
        $(this).toggleClass('active');
        
        /*if ( $('.subbrands').is(':hidden') ) {
            $('.subbrand-listing li').hide();
        }*/
        
        $(".notification-overlay").fadeToggle('fast', function() {
            var intTime = 0;
            $('.notification-listing li').each(function(i){
                var _this = this;
                window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
                intTime += 150;
            });              
        });
        
        $(".nano").nanoScroller({ iOSNativeScrolling: true });
    });    
    
    $(".icon-down-bold").click(function() {
        $(this).toggleClass('active');
        $(".store-detail-overlay").fadeToggle('fast', function() {
        
        /*var intTime = 0;
        $('.categories li').each(function(i){
            var _this = this;
            window.setTimeout(function(){$(_this).fadeIn('100');}, intTime);
            intTime += 150;
        });        
        */
            $(".nano").nanoScroller({ iOSNativeScrolling: true });
        });
    });
    
    
   $(".nano").nanoScroller({ iOSNativeScrolling: true });
    
    // 
    
    
	/* Use this js doc for all application specific JS */

	/* TABS --------------------------------- */
	/* Remove if you don't need :) */

	function activateTab($tab) {
		var $activeTab = $tab.closest('dl').find('a.active'),
				contentLocation = $tab.attr("href") + 'Tab';
				
		// Strip off the current url that IE adds
		contentLocation = contentLocation.replace(/^.+#/, '#');

		//Make Tab Active
		$activeTab.removeClass('active');
		$tab.addClass('active');

    //Show Tab Content
		$(contentLocation).closest('.tabs-content').children('li').hide();
		$(contentLocation).css('display', 'block');
	}

  $('dl.tabs dd a').on('click', function (event) {
    activateTab($(this));
  });

	if (window.location.hash) {
		activateTab($('a[href="' + window.location.hash + '"]'));
		$.foundation.customForms.appendCustomMarkup();
	}

	/* ALERT BOXES ------------ */
	$(".alert-box").delegate("a.close", "click", function(event) {
    event.preventDefault();
	  $(this).closest(".alert-box").fadeOut(function(event){
	    $(this).remove();
	  });
	});


	/* PLACEHOLDER FOR FORMS ------------- */
	/* Remove this and jquery.placeholder.min.js if you don't need :) */

	$('input, textarea').placeholder();

	/* TOOLTIPS ------------ */
	$(this).tooltips();



	/* UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE6/7/8 SUPPORT AND ARE USING .block-grids */
//	$('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'left'});
//	$('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'left'});
//	$('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'left'});
//	$('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'left'});



	/* DROPDOWN NAV ------------- */

	var lockNavBar = false;
	$('.nav-bar a.flyout-toggle').on('click', function(e) {
		e.preventDefault();
		var flyout = $(this).siblings('.flyout');
		if (lockNavBar === false) {
			$('.nav-bar .flyout').not(flyout).slideUp(500);
			flyout.slideToggle(500, function(){
				lockNavBar = false;
			});
		}
		lockNavBar = true;
	});
  if (!Modernizr.touch) {
    $('.nav-bar>li.has-flyout').hover(function() {
      $(this).children('.flyout').show();
    }, function() {
      $(this).children('.flyout').hide();
    })
  }


	/* DISABLED BUTTONS ------------- */
	/* Gives elements with a class of 'disabled' a return: false; */
  
  /* CUSTOM FORMS */
  $.foundation.customForms.appendCustomMarkup();
  
  
});
