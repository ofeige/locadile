<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
	
	/* Wrappers, Overlays, Live-Search */
	include("inc/wrapper.php");
?>

	<div class="wrapper">
		
		<div id="map-controler">
			<span id="zoom-in" class="jsb_ jsb_map_zoom_control"></span>
			<span id="zoom-out" class="jsb_ jsb_map_zoom_control"></span>
			<span id="localization" class="jsb_ jsb_map_localization"></span>
			<span id="fullscreen"></span>
		</div>		
		
		<div id="search">
			<!-- Search input result wrapper -->
			<div id="search-input-result-wrapper">
				<div id="search-input-wrapper" class="jsb_ jsb_search_input">
					<input type="text" placeholder="Search here..." id="search-input" autocomplete="off" />
				</div>
				<div id="search-result-wrapper" class="jsb_ jsb_search_suggestion"></div>
			</div>
			<!-- // Search input result wrapper -->

			<!-- Category input result wrapper -->
			<div id="category-input-result-wrapper">
				<div id="category-input" class="jsb_ jsb_category">... or use filter by category</div>
				<div id="category-result-wrapper"></div>
			</div>
			<!-- // Search category result wrapper -->

			<!-- Tag filter result wrapper -->
			<div id="tagfilter-button-result-wrapper">
				<div id="tagfilter-button" class="jsb_ jsb_tag"></div>
				<div id="tagfilter-result-wrapper"></div>				
			</div>
			<!-- // Tag filter result wrapper -->

			<div id="specialfilter-button-result-wrapper">
				<div id="specialfilter-button" class="jsb_ jsb_special" /></div>
				<div id="specialfilter-result-wrapper">
					<strong>Select specials you are interested in</strong>
				</div>			
			</div>
		</div>
	</div>

	<!-- Map -->
    <div id="map" class="fullscreen jsb_ jsb_map"></div>
    
<?php
	include_once("inc/foot.php");
?>