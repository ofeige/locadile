<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
	
	/* Wrappers, Overlays, Live-Search */
	include("inc/wrapper.php");
?>
    
    <!-- Flexslider -->
    <div id="flexslider-head" class="flexslider">
   
        <div class="wrapper">
            <div class="header-gallery-content">
            
                <div class="mb5">
                    <span class="button black-trans80 fs30 ttn">
                        KaDeWe
                    </span>
                </div>
                
                <div class="cf mb5">
                    <div class="float_l mr5">
                        <span class="button black-trans80 fs21 ttn">
                            Tauentzienstrasse 21-24
                        </span>
                    </div>
                    <div class="float_l">
                        <span class="button black-trans80 fs21 ttn">
                            10789 Berlin
                        </span>
                    </div>
                </div>
                
                <div class="cf mb5">
                    <div class="float_l mr5">
                        <a href="#" title="Email" class="button blue-trans80 fs18">
                            Email
                        </a>
                    </div>
                    <div class="float_l mr5">
                        <a href="#" title="Follow" class="button blue-trans80 fs18">
                            Follow
                        </a>
                    </div>
                    <div class="float_l">
                        <a href="#" title="Share" class="button blue-trans80 fs18">
                            Share
                        </a>
                    </div>                    
                </div> 
                
                <div class="mb5">
                    <span class="button white fs18 ttn open-now">Open now (2h 18m)</span>
                </div>
                
            </div>
        </div>   
        
        <div id="map" style="height: 750px; width: 100%; display: none;"></div>
        
        <!-- Header Gallery -->
        <ul class="slides cf">
            <li style="background-image: url(images/dummy/NewYork.jpeg);"></li>
            <li style="background-image: url(images/dummy/adidas_02.jpeg);"></li>
            <li style="background-image: url(images/dummy//AdidasStore.jpeg);"></li>
            <li style="background-image: url(images/dummy/Adidas-Store.jpeg);"></li>
                          
        </ul>
        <!-- // Header Gallery -->
   
    </div>
    <!-- // Flexslider -->
    
    <div class="fake-content hide-on-phones">
        <div class="row">
            <div class="twelve columns">
                <div id="flexslider-head-thumb">
                    
                    <ul class="slides cf">
                        <li style="background: url(images/dummy/NewYork.jpeg);"></li>
                        <li style="background: url(images/dummy/adidas_02.jpeg);"></li>
                        <li style="background: url(images/dummy/AdidasStore.jpeg);"></li>
                        <li style="background: url(images/dummy/Adidas-Store.jpeg);"></li>                        
                    </ul>
                    
                    <div id="showMap" style="background: url(images/map.jpg); width: 76px; height: 76px; float: right; margin-right: 35%; cursor: pointer;"></div>
                </div>        
            </div>
        </div>    
    </div>
    
    <!-- Subnavigation -->
    <div class="container">
        <div class="row">
            <div class="twelve columns store-navigation">
                <a href="store.php" title="Info" class="button blue-trans80 fs18 active">Info</a>
                <a href="specials.php" title="Specials" class="button blue-trans80 fs18">Specials <span class="count">(4)</span></a>
                <a href="brands.php" title="Brands" class="button blue-trans80 fs18">Brands <span class="count">(42)</span></a>
            </div>
        </div>
    </div>
    
    <!-- Description -->
    <div class="row">
        <div class="seven columns">
            <div class="white-bg description">
                <h3>Über uns</h3>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
                    sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem 
                    ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et 
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, 
                    no sea takimata sanctus est Lorem ipsum dolor sit amet.                    
                </p>
                <h3>Kategorien</h3>
                <ul class="categories cf">
                    <li>Books & Magazines</li>
                    <li>Furniture</li>
                    <li>Flowers & Nature</li>
                    <li>Fashion</li>
                    <li>Robots & Toys</li>
                    <li>Travel</li>
                </ul>
                
                <h3>Zahlungsarten</h3>
                <div class="row" style="margin-top: 12px;">
                    <div class="three columns">
                        <span class="type">Bar</span>
                        <ul class="payment cf ">
                            <li><i class="icon-feather white" title="Euro"></i></li>
                            <li><i class="icon-popup white" title="Dollar"></i></li>
                        </ul>
                    </div>
                    <div class="six columns">
                        <span class="type">Kartenzahlung</span>
                        <ul class="payment cf">
                            <li><i class="icon-paypal white" title="Paypal"></i></li>
                            <li><i class="icon-up-open white" title="Visa"></i></li>
                            <li><i class="icon-volume white" title="Mastercard"></i></li>
                            <li><i class="icon-network white" title="Maestro"></i></li>
                            <li><i class="icon-drive white" title="American Express"></i></li>
                        </ul>
                    </div>
                    <div class="three columns">
                        <span class="type">Andere</span>
                        <ul class="payment cf">
                            <li><i class="icon-cloud white" title="CH - Schweizer Franken"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- // seven columns -->
        <div class="five columns">
            <div class="white-bg description">
                <h3>Öffnungszeiten</h3>
                <table class="openings">
                    <tr>
                        <td>Mo</td>
                        <td>10:00h - 20:00h</td>
                    </tr>
                    <tr class="today">
                        <td>Dienstag</td>
                        <td>10:00h - 20:00h</td>
                    </tr>
                    <tr>
                        <td>Mi</td>
                        <td>10:00h - 20:00h</td>
                    </tr>
                    <tr>
                        <td>Do</td>
                        <td>10:00h - 20:00h</td>
                    </tr>
                    <tr>
                        <td>Fr</td>
                        <td>10:00h - 20:00h</td>
                    </tr>
                    <tr>
                        <td>Sa</td>
                        <td>09:30h - 22:00h</td>
                    </tr>                    
                </table>
                <h3>Social Media</h3>
                <ul class="payment cf">
                    <li><a href="http://facebook.com" title="Facebook" target="_blank"><i class="icon-facebook white"></i></a></li>
                    <li><a href="http://twitter.com" title="Twitter" target="_blank"><i class="icon-twitter white"></i></a></li>
                    <li><a href="http://vimeo.com" title="Vimeo" target="_blank"><i class="icon-vimeo white"></i></a></li>
                    <li><a href="http://linkedin.com" title="Linkedin" target="_blank"><i class="icon-linkedin white"></i></a></li>
                    <li><a href="http://dropbox.com" title="Dropbox" target="_blank"><i class="icon-dropbox white"></i></a></li>
                    <li><a href="http://skype.com" title="Skype" target="_blank"><i class="icon-skype white"></i></a></li>
                    <li><a href="http://tumblr.com" title="Tumblr" target="_blank"><i class="icon-tumblr white"></i></a></li>
                </ul>                
            </div>
        </div>
        <!-- // five columns -->
    </div>
    <!-- // row -->	
    
    <!-- logo slider -->
    <div class="row line">
        <div class="twelve columns">
            <!-- Flexslider -->
            <div class="flexslider overflow-visible four-items" style="margin-left: 0px; margin-right: -40px;">
              <ul class="slides cf">

                <li>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo1-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo2-215x215.jpg" />
                        </div>
                    </div>  
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo3-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo4-215x215.jpg" />
                        </div>
                    </div>                      
                </li>
                
                <li>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo1-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo2-215x215.jpg" />
                        </div>
                    </div>  
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo3-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo4-215x215.jpg" />
                        </div>
                    </div>                      
                </li>
                
                <li>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo1-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo2-215x215.jpg" />
                        </div>
                    </div>  
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo3-215x215.jpg" />
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="slide-item-box">
                            <img src="images/dummy/logo4-215x215.jpg" />
                        </div>
                    </div>                      
                </li>                
         
              </ul>            
            </div>
            <!-- // Flexslider -->
        </div>
    </div>    
    <!-- // logo slider -->
    
    <!-- footer -->
    <footer class="container">
        <div class="row">
            <div class="twelve columns">
                <ul class="copyright" style="width: 232px;">
                    <li>
                        <span class="locadile">locadile.<span>com</span></span>
                        &copy 2012 Locadile Inc.
                    </li>
                </ul>
                <ul class="features-benefits" style="width: 183px;">
                    <li><strong>Product Features & Benefits</strong></li>
                    <li><a href="#" title="Plans and Pricing">Plans and Pricing</a></li>
                    <li><a href="#" title="Benefits">Benefits</a></li>
                    <li><a href="#" title="locadile for Business Owners">locadile for Business Owners</a></li>
                    <li><a href="#" title="locadile for Brands">locadile for Brands</a></li>
                    <li><a href="#" title="locadile for Agencies">locadile for Agencies</a></li>
                    <li><a href="#" title="locadile for Developers">locadile for Developers</a></li>
                    <li><a href="#" title="locadile for Enterprises">locadile for Enterprises</a></li>
                </ul> 
                <ul class="customers-partners" style="width: 118px;">
                    <li><strong>Customers & Partners</strong></li>
                    <li><a href="#" title="Customers">Customers</a></li>
                    <li><a href="#" title="Partners">Partners</a></li>
                </ul>      
                <ul class="about" style="width: 44px;">
                    <li><strong>About</strong></li>
                    <li><a href="#" title="About">About</a></li>
                    <li><a href="#" title="Contact">Contact</a></li>
                    <li><a href="#" title="Jobs">Jobs</a></li>
                </ul>
                <ul class="resources" style="width: 105px;">
                    <li><strong>Resources</strong></li>
                    <li><a href="#" title="Knowledge Base">Knowledge Base</a></li>
                    <li><a href="#" title="What is locadile?">What is locadile?</a></li>
                    <li><a href="#" title="Getting Started">Getting Started</a></li>
                    <li><a href="#" title="FAQ">FAQ</a></li>
                    <li><a href="#" title="API Documentation">API Documentation</a></li>
                    <li><a href="#" title="Blog">Blog</a></li>                    
                </ul>     
                <ul class="other" style="width: 98px; margin-right: 0px;">
                    <li><strong>Other</strong></li>
                    <li><a href="#" title="About">Affiliate Programm</a></li>
                    <li><a href="#" title="Contact">Certified Partners</a></li>
                    <li><a href="#" title="Jobs">Request a Demo</a></li>                    
                </ul>                
            </div>
        </div>
    </footer>
    <!-- // footer -->    
<?php
	include_once("inc/foot.php");
?>