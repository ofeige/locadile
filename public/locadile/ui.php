<?php
	/* HTML HEAD SCRIPTE CSS */
	include("inc/head.php");
	
	/* Top-Bar */
	include("inc/top-bar.php");
?>

<style>
	#ui {
		margin: 65px 0 0;
		padding: 20px;
	}
	.button-nice {
		background-color: #009ddc;
		display: inline-block;
		width: 190px;
		height: 50px;
		color: #fff;
		text-align: center;
		line-height: 50px;
		border-radius: 3px;
		font-size: 18px;
		cursor: pointer;
	}
	.button-nice:hover {
		box-shadow: 0px 0px 5px #aaa;
	}
	.button-nice.disabled {
		background-color: #7fceed;
	}
	.button-nice.disabled:hover {
		box-shadow: none;
	}
	.button-nice.alert {
		background-color: #dc4800;
	}
	.button-nice.alert.disabled {
		background-color: #eda37f;
	}
</style>

<div id="ui" class="white-bg">
	<div class="wrapper">
		<span class="button-nice">Button</span>
		<span class="button-nice disabled">Disabled</span>
		<span class="button-nice alert">Alert</span>
		<span class="button-nice alert disabled">Disabled</span>		
	</div>	
</div>

<?php
	include_once("inc/footer.php");
	include_once("inc/foot.php");
?>