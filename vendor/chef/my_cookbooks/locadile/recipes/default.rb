require_recipe "apt"
require_recipe "apache2"
require_recipe "apache2::mod_rewrite"
require_recipe "apache2::mod_php5"
require_recipe "java"
require_recipe "elasticsearch"
require_recipe "elasticsearch-head"

web_app "locadile.com" do
  server_name node['hostname']
  server_aliases [node['fqdn'], "locadile.com"]
  docroot "/vagrant/public"
end

